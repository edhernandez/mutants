![Instruction Coverage 100%](https://img.shields.io/badge/Instruction%20Coverage%20-100%-green.svg)
![Branch Coverage 97%](https://img.shields.io/badge/Branch%20Coverage%20-97%-green.svg)

# Mutants Api #

![Spring Boot 1.5.7](https://img.shields.io/badge/Spring%20Boot%20-1.5.7-green.svg)
![Java 8](https://img.shields.io/badge/Java%20-8-orange.svg)
![MySql 5.7](https://img.shields.io/badge/MySql%20-5.7-blue.svg)

### ¿Para que es el repositorio? ###

Api rest diseñada para detectar si un humano es mutante

### ¿Que tengo que hacer para correrlo? ###
1. Crear una nueva base de datos de nombre mutant (junto con el usuario y la contraseña iguales)
2. La misma debe estar corriendo en el puerto 3306
3. Correr el script "schema.sql"
4. Correr el proyecto

### ¿Que apis tiene? ###
POST /mutant/: Analiza el adn enviado en formato de array de Strings. Devuelve 200 en caso de verificar un mutante, 403 en cualquier otro caso
GET /stats/: Obtener las estadisticas del uso del api

### Swagger UI ###
Para acceder a la consola de swagger ui debe ingresar a http://localhost:8080/swagger-ui.html. Alli encontraran mas detallado como se usa el api.

### Amazon AWS ###
Este api se encuentra hosteada en AWS. Para tener acceso a consumirla se debe acceder a http://mutantsmeli-env.pgpzzbd2ky.us-east-2.elasticbeanstalk.com/{uri-del-recurso}, por ejemplo http://mutantsmeli-env.pgpzzbd2ky.us-east-2.elasticbeanstalk.com/stats/

### URLs ###
#### Nivel 2 ####
```
POST GET http://mutantsmeli-env.pgpzzbd2ky.us-east-2.elasticbeanstalk.com/mutant/

body: {
  "dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
}
```
#### Nivel 3 ####
```
GET http://mutantsmeli-env.pgpzzbd2ky.us-east-2.elasticbeanstalk.com/stats/
```

### Diagama de clases ###
![Diagrama de clases](https://bytebucket.org/edhernandez/mutants/raw/5ac2b5c9ad7d44d6f11511a8677ac7d5b606b8b5/doc/diagrams/mutants.png?token=ced5eba000e6e17697acf1ca0aa14809cc92b412)

### Cobertura de Codigo ###
Para poder ver la cobertura del codigo, si estamos usando Netbeans, basta con hacer click derecho en el proyecto, ir a la opcion de "Run Maven" y seleccionar "coverage"
Sino, se debe ejecutar 
```
mvn clean test site
```