package com.mutants.repository;

import com.mutants.domain.Analysis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnalysisRepository extends JpaRepository<Analysis, Long> {

    long countByMutant(boolean mutant);

    Analysis findByDna(String dna);

}
