package com.mutants.service;

import com.mutants.domain.Stats;

public interface StatsService {

    Stats getStats();

}
