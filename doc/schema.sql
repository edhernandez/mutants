drop table if exists analysis;

create table analysis (
    id bigint UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    dna VARCHAR(255) unique not null,
    mutant boolean not null
);

