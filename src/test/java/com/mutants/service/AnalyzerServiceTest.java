package com.mutants.service;

import com.mutants.MutantsAbstractApplicationTests;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

public class AnalyzerServiceTest extends MutantsAbstractApplicationTests {

    @Autowired
    private AnalyzerService analyzerService;

    @Rule //i will use expectexception to check what is the message of exception
    public ExpectedException expectException = ExpectedException.none();

    @Test
    public void isMutant_withMutantDnaVertical_returnBoolean() {
        String[] dna = {
            "ACTGTG",
            "ACTCGC",
            "GACTCG",
            "AGCTCG",
            "GTTTAG",
            "TCCAAG"
        };
        boolean result = analyzerService.isMutant(dna);
        assertThat(result).isTrue();
    }

    @Test
    public void isMutant_withMutantDnaHorizontal_returnMutant() {
        String[] dna = {
            "ACTGTG",
            "ACTCGC",
            "GACTCA",
            "AGCTCG",
            "GTTTAG",
            "TCAAAA"
        };
        boolean result = analyzerService.isMutant(dna);
        assertThat(result).isTrue();
    }

    @Test
    public void isMutant_withMutantDnaDiagonalRightToLeft_returnMutant() {
        String[] dna = {
            "ACTGTG",
            "ACTCGC",
            "GACTCA",
            "AGCTAG",
            "GTTAAG",
            "TCAAGA"
        };
        boolean result = analyzerService.isMutant(dna);
        assertThat(result).isTrue();
    }

    @Test
    public void isMutant_withMutantDnaDiagonalLeftToRight_returnMutant() {
        String[] dna = {
            "ACTGTG",
            "GCTCGC",
            "GAATCA",
            "AGAACG",
            "GTTTAG",
            "TCAATA"
        };
        boolean result = analyzerService.isMutant(dna);
        assertThat(result).isTrue();
    }

    @Test
    public void isMutant_withHumanDna_returnHuman() {
        String[] dna = {
            "ACTGTG",
            "GCTCGC",
            "GAATCA",
            "AGAACG",
            "GTTTGG",
            "TCAATA"
        };
        boolean result = analyzerService.isMutant(dna);
        assertThat(result).isFalse();
    }

    @Test
    public void isMutant_withDnaWithoutOneRow_throwsException() {
        expectException.expect(IllegalArgumentException.class);
        expectException.expectMessage("You must enter an array of strings where the strings are the same length as the array.");
        String[] dna = {
            "ACTGTG",
            "GCTCGC",
            "GAATCA",
            "AGAACG",
            "GTTTGG"
        };
        analyzerService.isMutant(dna);
    }

    @Test
    public void isMutant_withDnaWithoutOneColumn_throwsException() {
        expectException.expect(IllegalArgumentException.class);
        expectException.expectMessage("You must enter an array of strings where the strings are the same length as the array.");
        String[] dna = {
            "ACTGT",
            "GCTCG",
            "GAATC",
            "AGAAC",
            "GTTTG",
            "TCAAT"
        };
        analyzerService.isMutant(dna);
    }

    @Test
    public void isMutant_withDnaWithInvalidNitrogenBase_throwsException() {
        expectException.expect(IllegalArgumentException.class);
        expectException.expectMessage("Check your nitrogen bases.");
        String[] dna = {
            "ACTGTT",
            "GCTCGG",
            "GAATCC",
            "AGAACC",
            "GTRTGG",
            "TCAATT"
        };
        analyzerService.isMutant(dna);
    }
}
