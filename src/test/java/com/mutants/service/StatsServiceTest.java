package com.mutants.service;

import com.mutants.MutantsAbstractApplicationTests;
import com.mutants.domain.Stats;
import java.math.BigDecimal;
import javax.transaction.Transactional;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Transactional
public class StatsServiceTest extends MutantsAbstractApplicationTests {

    @Autowired
    private StatsService statsService;

    @Test
    public void getStats_withMutantsAndHumansPersisted_returnStats() {
        Stats expectedStats = new Stats();
        expectedStats.setCountMutantDna(8);
        expectedStats.setCountHumanDna(4);
        expectedStats.setRatio(new BigDecimal(8 / 12));

        Stats stats = statsService.getStats();

        assertThat(stats).isEqualToComparingFieldByField(expectedStats);
    }

    @Test
    public void getStats_withoutMutantsAndHumansPersisted_returnStatsAll0() {
        Stats expectedStats = new Stats();
        expectedStats.setCountMutantDna(0);
        expectedStats.setCountHumanDna(0);
        expectedStats.setRatio(new BigDecimal(0));
        entityManager.createNativeQuery("delete from Analysis").executeUpdate();
        entityManager.flush();
        Stats stats = statsService.getStats();
        assertThat(stats).isEqualToComparingFieldByField(expectedStats);
    }

}
