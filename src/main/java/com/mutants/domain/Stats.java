package com.mutants.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mutants.serializer.RatioSerializer;
import java.math.BigDecimal;

public class Stats {

    @JsonProperty(value = "count_mutant_dna")
    private long countMutantDna;
    @JsonProperty(value = "count_human_dna")
    private long countHumanDna;
    @JsonSerialize(using = RatioSerializer.class)
    private BigDecimal ratio;

    public long getCountMutantDna() {
        return countMutantDna;
    }

    public void setCountMutantDna(long countMutantDna) {
        this.countMutantDna = countMutantDna;
    }

    public long getCountHumanDna() {
        return countHumanDna;
    }

    public void setCountHumanDna(long countHumanDna) {
        this.countHumanDna = countHumanDna;
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }

}
