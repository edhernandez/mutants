package com.mutants.controller.rest;

import com.mutants.domain.Analysis;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mutants.service.AnalysisService;

@RestController
@RequestMapping("/mutant")
public class MutantRestController {

    private final AnalysisService analysisService;

    public MutantRestController(AnalysisService analysisService) {
        this.analysisService = analysisService;
    }

    @PostMapping("/")
    public ResponseEntity<Analysis> checkAndSave(@RequestBody Map<String, String[]> body) {
        Analysis newAnalysis = analysisService.checkAndSave(body.get("dna"));
        HttpStatus httpStatus;
        if (newAnalysis.isMutant()) {
            httpStatus = HttpStatus.OK;
        } else {
            httpStatus = HttpStatus.FORBIDDEN;
        }
        return new ResponseEntity<Analysis>(newAnalysis, httpStatus);
    }

}
