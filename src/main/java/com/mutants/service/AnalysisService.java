package com.mutants.service;

import com.mutants.domain.Analysis;

public interface AnalysisService {

    Analysis checkAndSave(String[] dna);
}
