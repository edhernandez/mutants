package com.mutants;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class MutantsAbstractApplicationTests {

    @Autowired
    protected DataSource dataSource;

    @Autowired
    protected EntityManager entityManager;

}
