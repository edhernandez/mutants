package com.mutants.service.impl;

import com.mutants.service.AnalyzerService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class AnalyzerServiceImpl implements AnalyzerService {

    private final int[] directionRow = {0, 1, 1, 1};
    private final int[] directionCol = {1, 0, 1, -1};
    private String[] mutantSecuences = {"CCCC", "GGGG", "TTTT", "AAAA"};
    private List<Character> validChars;

    public AnalyzerServiceImpl() {
        validChars = new ArrayList<Character>();
        validChars.add('A');
        validChars.add('C');
        validChars.add('G');
        validChars.add('T');
    }

    @Override
    public boolean isMutant(String[] dna) {
        validateDna(dna);
        int rows = dna.length;
        int cols = dna.length;
        for (int actualRowPosition = 0; actualRowPosition < rows; actualRowPosition++) {
            for (int actualColumnPosition = 0; actualColumnPosition < cols; actualColumnPosition++) {
                if (isMutant(dna, actualRowPosition, actualColumnPosition)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isMutant(String[] dna, int actualRowPosition, int actualColumnPosition) {
        if (validChars.contains(dna[actualRowPosition].charAt(actualColumnPosition))) {
            for (String secuence : mutantSecuences) {
                if (findSecuenceInDna(dna, secuence, actualRowPosition, actualColumnPosition)) {
                    return true;
                }
            }
        } else {
            throw new IllegalArgumentException("Check your nitrogen bases.");
        }
        return false;
    }

    private boolean findSecuenceInDna(String[] dna, String secuence, int actualRowPosition, int actualColumnPosition) {
        if (secuence.charAt(0) == dna[actualRowPosition].charAt(actualColumnPosition)) {
            for (int k = 0; k < directionRow.length; k++) {
                if (search(dna, secuence, actualRowPosition, actualColumnPosition, 0, directionRow[k], directionCol[k])) {
                    return true;
                }
            }
        }
        return false;
    }

    private void validateDna(String[] dna) {
        int numberOfRows = dna.length;

        for (String dnaChain : dna) {
            if (dnaChain.length() != numberOfRows) {
                throw new IllegalArgumentException("You must enter an array of strings where the strings are the same length as the array.");
            }
        }
    }

    private boolean search(String[] dna, String dnaChain, int row, int col, int actualPosition, int rowDirection, int columnDirection) {
        if (actualPosition == dnaChain.length()) {
            return true;
        }
        if (!isInBounds(row, col, dna.length)) {
            return false;
        }

        if (dna[row].charAt(col) == dnaChain.charAt(actualPosition)) {
            return search(dna, dnaChain, row + rowDirection, col + columnDirection, actualPosition + 1, rowDirection, columnDirection);
        }
        return false;
    }

    private boolean isInBounds(int rowPosition, int colPosition, int length) {
        return rowPosition >= 0 && colPosition >= 0 && rowPosition < length && colPosition < length;
    }
}
