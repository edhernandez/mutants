package com.mutants.service.impl;

import com.mutants.domain.Stats;
import com.mutants.service.StatsService;
import java.math.BigDecimal;
import org.springframework.stereotype.Service;
import com.mutants.repository.AnalysisRepository;

@Service
public class StatsServiceImpl implements StatsService {

    private final AnalysisRepository analysisRepository;

    public StatsServiceImpl(AnalysisRepository analysisRepository) {
        this.analysisRepository = analysisRepository;
    }

    @Override
    public Stats getStats() {

        Stats expectedStats = new Stats();
        analysisRepository.findOne(1l);
        long mutantQuantity = analysisRepository.countByMutant(true);
        long humanQunatity = analysisRepository.countByMutant(false);
        expectedStats.setCountMutantDna(mutantQuantity);
        expectedStats.setCountHumanDna(humanQunatity);
        long total = (mutantQuantity + humanQunatity);
        if (total == 0) {
            expectedStats.setRatio(new BigDecimal(0));

        } else {
            expectedStats.setRatio(new BigDecimal(mutantQuantity / total));
        }
        return expectedStats;
    }

}
