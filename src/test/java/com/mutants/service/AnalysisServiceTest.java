package com.mutants.service;

import com.mutants.MutantsAbstractApplicationTests;
import com.mutants.domain.Analysis;
import javax.transaction.Transactional;
import org.apache.commons.codec.digest.DigestUtils;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;

@Transactional
public class AnalysisServiceTest extends MutantsAbstractApplicationTests {

    @Autowired
    private AnalysisService analysisService;
    @Rule //i will use expectexception to check what is the message of exception
    public ExpectedException expectException = ExpectedException.none();

    @Test
    public void checkAndSave_withMutantDna_returnTrueAndPersistAnalysis() {
        String[] dna = {
            "ACTGTG",
            "ACTCGC",
            "GACTCG",
            "AGCTCG",
            "GTTTAG",
            "TCCAAG"
        };
        String jpaQuery = "select count(a) from Analysis a";

        Analysis expectedMutant = new Analysis();
        expectedMutant.setMutant(true);
        expectedMutant.setDna(DigestUtils.sha256Hex(String.join("", dna)));

        long mutantsBefore = (long) entityManager.createQuery(jpaQuery).getSingleResult();

        Analysis mutant = analysisService.checkAndSave(dna);

        long mutantsAfter = (long) entityManager.createQuery(jpaQuery).getSingleResult();
        assertThat(mutantsAfter).isEqualTo(mutantsBefore + 1);
        assertThat(mutant).isNotNull().isEqualToIgnoringNullFields(expectedMutant);
    }

    @Test
    public void checkAndSave_withHumanDna_returnFalseAndPersistAnalysis() {
        String[] dna = {
            "ACTGTG",
            "ACTCGC",
            "GACTCG",
            "AGCTCG",
            "GTTTAT",
            "TCCAAG"
        };
        String jpaQuery = "select count(a) from Analysis a";

        Analysis expectedMutant = new Analysis();
        expectedMutant.setMutant(false);
        expectedMutant.setDna(DigestUtils.sha256Hex(String.join("", dna)));

        long mutantsBefore = (long) entityManager.createQuery(jpaQuery).getSingleResult();

        Analysis mutant = analysisService.checkAndSave(dna);

        long mutantsAfter = (long) entityManager.createQuery(jpaQuery).getSingleResult();

        assertThat(mutantsAfter).isEqualTo(mutantsBefore + 1);
        assertThat(mutant).isNotNull().isEqualToIgnoringNullFields(expectedMutant);
    }

    @Test
    public void checkAndSave_withInvalidDna_throwException() {
        expectException.expect(IllegalArgumentException.class);
        expectException.expectMessage("You must enter an array of strings where the strings are the same length as the array.");
        String[] dna = {
            "ACTGTG",
            "GCTCGC",
            "GAATCA",
            "AGAACG",
            "GTTTGG"
        };
        analysisService.checkAndSave(dna);
    }

    @Test
    public void checkAndSave_withDnaWithInvalidNitrogenBases_throwsException() {
        expectException.expect(IllegalArgumentException.class);
        expectException.expectMessage("Check your nitrogen bases.");
        String[] dna = {
            "ACTGTT",
            "GCTCGG",
            "GAATCC",
            "AGAACC",
            "GTRTGG",
            "TCAATT"
        };
        analysisService.checkAndSave(dna);
    }

    @Test
    public void checkAndSave_withDnaAlreadyPersist_returnResultPersisted() {
        String[] dna = {
            "ATGCGA",
            "CAGTGC",
            "TTGTTT",
            "AGACGG",
            "GAGTCA",
            "TCGCTG"
        };
        Analysis mutant = analysisService.checkAndSave(dna);
        assertThat(mutant).hasFieldOrPropertyWithValue("id", 10l);
    }

}
