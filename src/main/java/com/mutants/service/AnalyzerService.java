package com.mutants.service;

public interface AnalyzerService {

    boolean isMutant(String[] dna);
}
