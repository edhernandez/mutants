package com.mutants.service.impl;

import com.mutants.domain.Analysis;
import com.mutants.service.AnalyzerService;
import javax.transaction.Transactional;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;
import com.mutants.service.AnalysisService;
import com.mutants.repository.AnalysisRepository;

@Service
@Transactional
public class AnalysisServiceImpl implements AnalysisService {

    private final AnalyzerService analyzerService;
    private final AnalysisRepository analysisRepository;

    public AnalysisServiceImpl(AnalyzerService analyzerService, AnalysisRepository analysisRepository) {
        this.analyzerService = analyzerService;
        this.analysisRepository = analysisRepository;
    }

    @Override
    public Analysis checkAndSave(String[] dna) {
        Analysis analysis = new Analysis();

        boolean isMutant = analyzerService.isMutant(dna);
        analysis.setMutant(isMutant);
        analysis.setDna(DigestUtils.sha256Hex(String.join("", dna)));

        Analysis existingMutant = analysisRepository.findByDna(analysis.getDna());
        if (existingMutant == null) {
            return analysisRepository.save(analysis);
        }
        return existingMutant;
    }

}
